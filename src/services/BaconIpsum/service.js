import api from '../base';

export default {
  getBaconIpsum(params) {
    return api.get(`/?type=${params.type}&paras=${params.numParagraphs}&sentences=${params.numSentences}&start-with-lorem=${params.startWithLorem}&format=${params.format}`);
  }
};
