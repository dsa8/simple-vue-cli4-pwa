import axios from 'axios';
import ENVIRONMENT from '../../env';

const config = axios.create({
  baseURL: ENVIRONMENT.endpoint,
  timeout: 200000
});

export default config;
