import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/services/BaconIpsum/service';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    BaconIpsum: '',
    IsOnline: true,
  },
  mutations: {
    updateBaconIpsum (state, newBaconIpsum) {
      state.BaconIpsum = newBaconIpsum;
    },
    updateIsOnline (state, newIsOnline) {
      state.IsOnline = newIsOnline;
    }
  },
  actions: {
    async getNewBaconIpsum ({ commit }, params) {
      try {
        const response = await api.getBaconIpsum(params);

        commit('updateBaconIpsum', response.data[0]);
        commit('updateIsOnline', navigator.onLine);
      } catch (e) {
        console.log(e.message);
      }
    }
  },
  modules: {
  }
})
