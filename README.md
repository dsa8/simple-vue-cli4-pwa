# default-vue-pwa

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## PWA
##### Simple pwa example, using workbox GenerateSW plugin to generate the service worker.
##### Current features include:
* Offline mode, using cache first stratagy
* Desktop mode
* Simple runtime caching for API request

### Build and run to test pwa
```
npm run build
```
```
serve -s dist
```