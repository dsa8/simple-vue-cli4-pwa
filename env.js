export default {
  production: {
    endpoint: 'https://baconipsum.com/api'
  },

  development: {
    endpoint: 'https://baconipsum.com/api'
  }
}[process.env.NODE_ENV];
