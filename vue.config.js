module.exports = {
    devServer: { https: true },

    pwa: {
        name: 'Test PWA',
        themeColor: '#4DBA87',
        msTileColor: '#000000',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black',

        // configure the workbox plugin
        workboxOptions: {
            cleanupOutdatedCaches: true,
            runtimeCaching: [
                {
                    urlPattern: /^https:\/\/baconipsum.com\/api*/,
                    handler: "networkFirst",
                    options: {
                        cacheName: "apiResponse",
                        expiration: {
                            maxAgeSeconds: 60 * 60 * 24
                        }
                    }
                }
            ]
        }
    }
}